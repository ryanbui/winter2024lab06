import java.util.Scanner;

public class LuckyCardGameApp {
	public static void main (String [] args){
		GameManager manager = new GameManager();
		int numberOfRounds = 1;
		int totalPoints = 0;
		String starLine = "****************************";
		while(totalPoints < 5 && manager.getNumberOfCards() > 1){
			System.out.println("Round: " + numberOfRounds);
			System.out.println(manager);
			totalPoints += manager.calculatePoints();
			System.out.println("Your points after round " + numberOfRounds + ": " + totalPoints + "\n" + starLine);
			manager.dealCards();
			numberOfRounds++;
			if(manager.getNumberOfCards() == 0){
				System.out.println("Round: " + numberOfRounds);
				System.out.println(manager);
				totalPoints += manager.calculatePoints();
				System.out.println("Your points after round " + numberOfRounds + ": " + totalPoints + "\n" + starLine);
			}
		}
		if(totalPoints >= 5){
			System.out.println("Player wins with: " + totalPoints + " points");
		} else {
			System.out.println("Player loses with: " + totalPoints + " points");
		}
	}
}