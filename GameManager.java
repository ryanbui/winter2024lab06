public class GameManager {
    private Deck drawPile;
    private Card centerCard;
    private Card playerCard;

    public GameManager() {
        this.drawPile = new Deck();
        this.drawPile.shuffle();
        this.centerCard = drawPile.drawTopCard();
        this.drawPile.shuffle();
        this.playerCard = drawPile.drawTopCard();
    }
    
    public String toString() {
        String dottedLine = "------------------------------";
        String result = dottedLine + "\n" + "Center Card: " + this.centerCard.toString() + "\n" + "Player Card: " 
        + this.playerCard.toString() + "\n" + dottedLine;
        return result;
    }

    public void dealCards(){
        this.drawPile.shuffle();
        this.centerCard = drawPile.drawTopCard();
        this.drawPile.shuffle();
        this.playerCard = drawPile.drawTopCard();
    }

    public int getNumberOfCards(){
        return drawPile.length();
    }

    public int calculatePoints(){
        if(this.centerCard.getValue() == this.playerCard.getValue()){
            return 4;
        } else if(this.centerCard.getSuit() == this.playerCard.getSuit()){
            return 2;
        } else {
            return -1;
        }
    }
}
